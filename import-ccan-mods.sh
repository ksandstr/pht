#!/bin/sh
set -e
for mod in $@; do
	echo ": ext/ccan/ccan/${mod}/${mod}.c |> !cc |> ccan-${mod}.o"
done
