## What

tl;dr: Progressively rebuilt hashed multiset. Interface not unlike CCAN htable.
Pointer-munging, therefore incompatible w/ valgrind and libgc. GPLv3-or-later.

## Status

Currently "pht" is feature complete. Unless a significant new optimization
comes along, further development is limited to improving its test and
benchmarking suites.

**Be warned**: there is a bug in the migration algorithm which will sometimes
hide items that should otherwise be found. This can sometimes be provoked in
the benchmark program with debugs enabled and given the cracklib 190k-line
wordlist for input.

## How

A more thorough treatment of the design may appear here at a later date, but
for now just use the source. There are also tests and a big old benchmark
which produces meaningless results when not built under `CONFIG_DEBUG=n` in
tup.config, or when run unlike how the script provided would.

The core idea, relayed by someone on Reddit years ago (mail me for credit if
it was you, or you have the link), is that of handling hash table growth
progressively to gain a vastly superior maximum bound for the insert operation.
Compared to CCAN htable this amounts to 2-3% fewer total clock cycles spent
in a table-building benchmark while fetch/del measurements are somewhat worse
due to an extra indirection. This comes at the expense of 1.5x space, the
correct tradeoff for interactive programs in the 21th century, and an extra
probe's worth of cycles spent in some positive and all negative lookups to
cover secondary tables.

## Why

The practical reason for this data structure is to demonstrate a low-latency
alternative to htables in designs where a mutexed htable's big rebuild loops
cause systemwide knock-on sleeping. There is currently no multi-threaded
benchmark to display such an advantage.

The fanciful reason is that even if my lock-free/wait-free hash table "lfht"
had a sound migration algorithm (which it might not), its higher-level design
is nevertheless lacking in guarantees wrt space and time. This project
therefore explores that aspect, before lfht gets a proper refit, before it is
in turn applied in the transactional memory library.

## Core brags

The inter-table migration algorithm for this data structure, consisting of an
"inner" algorithm that avoids rehashing while migrating items and an "outer"
driver that increases cache utilization, is believed novel in its design and
implementation as of November 2020, nyah nyah, neener neener.

  -- Kalle A. Sandström <ksandstr@iki.fi>
