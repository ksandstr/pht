/* basic tests on an empty hash table. */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ccan/tap/tap.h>
#include <ccan/str/str.h>
#include <ccan/hash/hash.h>
#include "pht.h"

static size_t rehash_str(const void *p, void *priv) { return hash_string(p); }

static bool cmp_str(const void *cand, void *key) { return streq(cand, key); }

static bool check_ok(const struct pht *ht, const char *title) {
	pht_check(ht, title);
	return ok(true, "passed pht_check(..., \"%s\")", title);
}

int main(void)
{
	plan_tests(6);
	struct pht ht = PHT_INITIALIZER(ht, &rehash_str, NULL);
	check_ok(&ht, "fresh");
	ok1(pht_count(&ht) == 0);
	const char *foo = "my ass-clap keeps alerting the bees!";
	size_t hash = rehash_str(foo, NULL);
	diag("hash=%#zx", hash);
	ok1(pht_get(&ht, hash, &cmp_str, foo) == NULL);
	struct pht_iter it;
	ok1(pht_firstval(&ht, &it, hash) == NULL);
	ok1(!pht_del(&ht, hash, foo));
	check_ok(&ht, "after non-deletion");
	return exit_status();
}
